---
layout: page
title: Download
konqi: /assets/img/konqi-mail.png
css-include: /css/download.css

sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        Kontact is already available on majority of Linux distributions. You
        can install it from the KDE Software Center.
  - name: Flatpak
    icon: /assets/img/flatpak.png
    description: >
        You can install the latest <a href="https://flathub.org/apps/details/org.kde.kontact">
        Kontact Flatpak</a> from Flathub. Experimental Flatpaks with nightly builds of Kontact
        can be <a href="https://community.kde.org/KDE_PIM/Flatpak">installed from the KDE Flatpak repository</a>.
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        Kontact is released regularly as part of KDE Applications. Kontact
        consists of many separate components, all of which you can find among
        <a href="https://download.kde.org/stable/applications">tarballs from
        the latest KDE Applications release</a>.

        If you want to build Kontact from sources, we recommend checking our
        <a href="get-involved.html">Getting Involved</a> page which contains
        links to full guide how to compile all Kontact components.
  - name: Git
    icon: /assets/img/git.svg
    description: >
        Kontact is spread across a multitude of repositories. Check our
        <a href="get-involved.html">Getting Involved page</a> for detailed
        instructions how to get all Kontact repositories.
  - name: Windows
    description: >
        We are currently working on bringing Kontact to Windows. Do you want
        to help us? Check <a href="get-involved.html">how to get involved</a>
        and get in touch with us!
---

<h1>Download</h1>

<table class="distribution-table">
{% for source in page.sources %}
    <tr class="title-row">
        <td rowspan="2" width="100">
            <img src="{{ source.icon }}" alt="{{ source.name }}">
        </td>
        <th>{{ source.name }}</th>
    </tr>
    <tr>
        <td>{{ source.description }}</td>
    </tr>
{% endfor %}
</table>
